//
//  Meal.swift
//  FoodTracker
//
//  Created by Daniel Macedo on 02/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//
import UIKit

struct Meal {
    var name:String?
    var thumbnail:UIImage?
    var rating:Int?
}
