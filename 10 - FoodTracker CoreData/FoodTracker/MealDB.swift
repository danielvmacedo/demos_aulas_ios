//
//  MealDB.swift
//  FoodTracker
//
//  Created by Daniel Macedo on 02/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import Foundation
import CoreData

class MealDB {
    static let instance = MealDB()
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MealModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func newMeal() -> Meal? {
        let meal = NSEntityDescription.insertNewObject(forEntityName: "Meal", into: self.persistentContainer.viewContext) as? Meal
        
        return meal
    }
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func allMeals() -> [Meal] {
        
        let request: NSFetchRequest<Meal> = Meal.fetchRequest()
        
        do {
            let searchResults = try self.persistentContainer.viewContext.fetch(request)
            
            return searchResults
            
        } catch {
            print("Error with request: \(error)")
        }
        
        return [Meal]()
    }
    
    func delete(meal:Meal) -> Void {
        self.persistentContainer.viewContext.delete(meal)
    }

}
