//
//  MealTableViewCell.swift
//  FoodTracker
//
//  Created by Daniel Macedo on 02/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class MealTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mealThumbnail: UIImageView!

    @IBOutlet weak var mealName: UILabel!
    
    @IBOutlet var ratingButtons: [UIButton]!
    
    private var _rating = 0
    
    var rating:Int {
        get {
            return _rating
        }
        
        set {
            _rating = newValue
            self.updateRatingButtons()
        }
    }
    
    @IBAction func rate(_ sender: UIButton) {
        self._rating = self.ratingButtons.index(of: sender)! + 1
        
        self.updateRatingButtons()
    }
    
    func updateRatingButtons() {
        for (index, button) in self.ratingButtons.enumerated() {
            if(index < self._rating) {
                button.isSelected = true
            } else {
                button.isSelected = false
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        for button in self.ratingButtons {
            let emptyStar = UIImage(named: "emptyStar")
            let filledStar = UIImage(named: "filledStar")
            
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(filledStar, for: .highlighted)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
