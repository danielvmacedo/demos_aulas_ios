//
//  Meal+CoreDataProperties.swift
//  FoodTracker
//
//  Created by Daniel Macedo on 02/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import Foundation
import CoreData


extension Meal {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Meal> {
        return NSFetchRequest<Meal>(entityName: "Meal");
    }

    @NSManaged public var name: String?
    @NSManaged public var thumbnail: NSData?
    @NSManaged public var rating: Int32

}
