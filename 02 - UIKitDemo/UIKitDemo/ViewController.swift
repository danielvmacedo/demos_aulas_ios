//
//  ViewController.swift
//  UIKitDemo
//
//  Created by Daniel Macedo on 25/11/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var myProgress: UIProgressView!
    @IBOutlet weak var myImage: UIImageView!
    
    @IBAction func changeLabelText(_ sender: UIButton) {
        self.myLabel.text = "Hello World!"
    }
    
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        print("Segmented Value: \(sender.selectedSegmentIndex)")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("Text: \(textField.text!)")
        return true
    }
    
    @IBAction func sliderChnaged(_ sender: UISlider) {
        self.myProgress.progress = sender.value
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        self.myImage.isHidden = !sender.isOn
    }
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        print("\(sender.date.description)")
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "iOS"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

