//
//  PokedexTableViewController.swift
//  Pokedex
//
//  Created by Daniel Macedo on 03/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit
import Alamofire

class PokedexTableViewController: UITableViewController {

    var pokemons = [Pokemon]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.requestPokemons()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func requestPokemons() -> Void {
        Alamofire.request("http://pokeapi.co/api/v2/pokemon/?limit=151").responseJSON { response in
            
            self.pokemons.removeAll()
            
            if let json = response.result.value as? [String:Any] {
                for (index, pokemon) in (json["results"] as! [[String:Any]]).enumerated() {
                    let newPokemon = Pokemon(number: index+1, name: pokemon["name"] as? String, url: pokemon["url"] as? String)
                    self.pokemons.append(newPokemon)
                }
            }
            
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.pokemons.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PokedexCell", for: indexPath)
        cell.textLabel?.text = "#\(indexPath.row + 1) - \(self.pokemons[indexPath.row].name!.capitalized)"
        cell.imageView?.image = UIImage(named: "\(self.pokemons[indexPath.row].number!)")
        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowPokemon") {
            if let dvc = segue.destination as? PokemonViewController {
                let indexPath = self.tableView.indexPath(for: sender as! UITableViewCell)
                dvc.pokemon = self.pokemons[indexPath!.row]
            }
        }
    }
    

}
