//
//  TelaUmViewController.swift
//  HelloNavigationController
//
//  Created by Daniel Macedo on 26/11/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class TelaUmViewController: UIViewController {
    
    
    @IBOutlet weak var myTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "TelaDoisSegue") {
            if let dvc = segue.destination as? TelaDoisViewController {
                dvc.myText = self.myTextField.text!
            }
        }
    }
    

}
