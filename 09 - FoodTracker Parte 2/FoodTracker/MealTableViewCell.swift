//
//  MealTableViewCell.swift
//  FoodTracker
//
//  Created by Daniel Macedo on 02/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class MealTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mealThumbnail: UIImageView!

    @IBOutlet weak var mealName: UILabel!
    
    @IBOutlet weak var ratingControl: RatingControlView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
