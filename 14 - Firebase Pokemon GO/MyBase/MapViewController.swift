//
//  MapViewController.swift
//  MyBase
//
//  Created by Daniel Macedo on 09/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class MapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!
    
    private var firstZoom = false
    
    var locationManager = CLLocationManager()
    
    private var user:FIRUser?
    
    var ref: FIRDatabaseReference!
    var refUsers: FIRDatabaseReference!
    var refPokemons: FIRDatabaseReference!
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.map.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if !firstZoom {
            let newRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 2000, 2000)
            self.map.setRegion(newRegion, animated: true)
        }
    }
    
    @IBAction func signOut(_ sender: UIBarButtonItem) {
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
            self.dismiss(animated: true, completion: nil)
        } catch let signOutError as NSError {
            self.present(buildAlertController(withMessage: signOutError.localizedDescription), animated: true, completion: nil)
        }
    }
    
    @IBAction func mapTouch(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.map)
        let coordinates = self.map.convert(location, toCoordinateFrom: self.map)
        print(coordinates)
    }
    
    func setupPokemonWatcher() -> Void {
        self.refPokemons.observeSingleEvent(of:.value, with: { (snapshot) in
            print("- Start -")
            self.map.removeAnnotations(self.map.annotations)
            for childSnapshot in snapshot.children {
                let child = childSnapshot as! FIRDataSnapshot
                let value = child.value as! [String:Any]
                
                let pokemonId = value["id"] as! Int
                let pokemonName = value["name"] as! String
                let pokemonLat = value["lat"] as! Float
                let pokemonLong = value["long"] as! Float
                let pokemon = Pokemon(name: pokemonName, coordinate: CLLocationCoordinate2DMake(CLLocationDegrees(pokemonLat), CLLocationDegrees(pokemonLong)))
                pokemon.id = pokemonId
                pokemon.ref = child.ref
                
                self.map.addAnnotation(pokemon)
                
            }
            print("- End -")
        })
        
        self.refPokemons.observe(.childAdded, with: { (snapshot) in
            let value = snapshot.value as! [String: Any]
            
            let pokemonId = value["id"] as! Int
            let pokemonName = value["name"] as! String
            let pokemonLat = value["lat"] as! Float
            let pokemonLong = value["long"] as! Float
            let pokemon = Pokemon(name: pokemonName, coordinate: CLLocationCoordinate2DMake(CLLocationDegrees(pokemonLat), CLLocationDegrees(pokemonLong)))
            pokemon.id = pokemonId
            pokemon.ref = snapshot.ref
            
            self.map.addAnnotation(pokemon)
            
        })
        
        self.refPokemons.observe(.childRemoved, with: { (snapshot) in

            for annotation in self.map.annotations {
                if let pokemon = annotation as? Pokemon {
                    if(pokemon.ref!.key == snapshot.ref.key) {
                        self.map.removeAnnotation(annotation)
                    }
                }
            }
            
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.map.delegate = self
        self.map.showsUserLocation = true
        checkLocationAuthorizationStatus()
        
        if let user = FIRAuth.auth()?.currentUser {
            self.user = user
            print("Welcome \(self.user!.email)")
            ref = FIRDatabase.database().reference()
            refUsers = ref.child("users")
            refPokemons = ref.child("pokemons")
        }
        
        self.setupPokemonWatcher()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Pokemon {
            let identifier = "pin"
            var view: MKAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
                dequeuedView.annotation = annotation
                view = dequeuedView
                view.image = UIImage(named: "\(annotation.id)")
            } else {
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.image = UIImage(named: "\(annotation.id)")
            }
            return view
        }
        return nil
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) -> Void {
        print("AAAAAAAA")
        let annotation = view.annotation!
        if let pokemon = annotation as? Pokemon {
            self.refUsers.child(self.user!.uid).childByAutoId().setValue(pokemon.toAnyObject())
            pokemon.ref?.removeValue()
            self.map.removeAnnotation(annotation)
            
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
