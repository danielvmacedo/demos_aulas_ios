//
//  LoginViewController.swift
//  MyBase
//
//  Created by Daniel Macedo on 09/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FIRAuth.auth()?.addStateDidChangeListener() { (auth, user) in
            if let _ = user {
                self.performSegue(withIdentifier: "MapScreen", sender: nil)
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(_ sender: UIButton) {
        FIRAuth.auth()?.signIn(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
            if let errorResult = error {
                self.present(buildAlertController(withMessage: errorResult.localizedDescription), animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func signup(_ sender: UIButton) {
        FIRAuth.auth()?.createUser(withEmail: self.email.text!, password: self.password.text!) { (user, error) in
            
            if let errorResult = error {
                self.present(buildAlertController(withMessage: errorResult.localizedDescription), animated: true, completion: nil)
            }
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
