//
//  PokemonViewController.swift
//  Pokedex
//
//  Created by Daniel Macedo on 03/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit
import Alamofire

class PokemonViewController: UIViewController {
    
    @IBOutlet weak var thumbnail: UIImageView!

    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var evolutionLabel: UILabel!
    
    var pokemon:Pokemon?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let pokemon = self.pokemon {
            self.thumbnail.image = UIImage(named: "\(pokemon.number!)")
            self.title = pokemon.name!.capitalized
        }
        
        self.requestPokemon()
        // Do any additional setup after loading the view.
    }

    func requestPokemon() -> Void {
        let evolutionChainURL = "http://pokeapi.co/api/v1/pokemon/\(self.pokemon!.number!)"
        print(evolutionChainURL)
        Alamofire.request(evolutionChainURL).responseJSON { response in
            if let json = response.result.value as? [String:Any] {
                //Get Pokemon Type
                let types = json["types"] as! [[String:Any]]
                var typesString = ""
                for type in types {
                    let typeName = (type["name"] as! String).capitalized
                    if(typesString == "") {
                        typesString += typeName
                    } else {
                        typesString += "/"
                        typesString += typeName
                    }
                }
                
                self.typeLabel.text = typesString
                
                
                //Get Pokemon Evolution
                let evolutions = json["evolutions"] as! [[String:Any]]
                
                if(evolutions.count > 0) {
                    let evolution = evolutions[0]
                    let evolutionName = (evolution["to"] as! String).capitalized
                    self.evolutionLabel.text = evolutionName
                    
                } else {
                    self.evolutionLabel.text = "No evolution"
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
