//
//  Pokemon.swift
//  MyBase
//
//  Created by Daniel Macedo on 09/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class Pokemon: NSObject, MKAnnotation {
    var id:Int = 0
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var ref: FIRDatabaseReference?
    var name:String? {
        get {
            return self.title
        }
    }
    
    init(name:String?, coordinate:CLLocationCoordinate2D) {
        self.title = name
        self.coordinate = coordinate
    }
    
    func toAnyObject() -> Any {
        return ["id":self.id,"lat":Float(coordinate.latitude), "long":Float(coordinate.longitude), "name":self.title!]
    }
}
