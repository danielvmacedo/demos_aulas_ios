//
//  ViewController.swift
//  Calculadora
//
//  Created by Daniel Macedo on 26/11/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var valueLabel: UILabel!
    
    private var value:Float = 0.0
    private var acumulator:Float = 0.0
    
    private var isOperation:Bool = false
    
    private var calcOperator:String = "="

    @IBAction func numberInput(_ sender: UIButton) {
        let number = Float(sender.titleLabel!.text!)!
        
        self.value = self.value * 2 + number
        self.valueLabel.text = "\(self.value)"
    }
    
    @IBAction func clearCalculator(_ sender: UIButton) {
        self.value = 0.0
        self.acumulator = 0.0
        self.valueLabel.text = "\(self.value)"
    }
    
    @IBAction func operatorInput(_ sender: UIButton) {
        
        switch self.calcOperator {
        case "=":
            print("Equal")
            self.acumulator = self.value
        case "/":
            print("Divide")
            self.acumulator = self.acumulator / self.value
        case "*":
            print("Multiply")
            self.acumulator = self.acumulator * self.value
        case "-":
            print("Subtract")
            self.acumulator = self.acumulator - self.value
        case "+":
            print("Add")
            self.acumulator = self.acumulator + self.value
        default:
            print("None")
        }
        
        self.value = 0.0
        self.valueLabel.text = "\(self.acumulator)"
        
        if(sender.titleLabel!.text == "=") {
            self.acumulator = 0.0
        }
        
        self.calcOperator = sender.titleLabel!.text!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

