//
//  MealViewController.swift
//  FoodTracker
//
//  Created by Daniel Macedo on 02/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class MealViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var thumbnail: UIImageView!
    
    @IBOutlet weak var rating: RatingControlView!
    
    var meal:Meal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let meal = self.meal {
            self.name.text = meal.name
            self.thumbnail.image = meal.thumbnail
            self.rating.rating = meal.rating!
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func chooseThumbnail(_ sender: UITapGestureRecognizer) {
        let imgPicker = UIImagePickerController()
        imgPicker.sourceType = .photoLibrary
        imgPicker.allowsEditing = true
        imgPicker.delegate = self
        
        self.present(imgPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let selectedImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        self.thumbnail.image = selectedImage
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Save" {
            self.meal = Meal(name: self.name.text, thumbnail: self.thumbnail.image, rating: self.rating.rating)
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
