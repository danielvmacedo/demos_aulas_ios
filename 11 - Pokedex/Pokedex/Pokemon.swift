//
//  Pokemon.swift
//  Pokedex
//
//  Created by Daniel Macedo on 03/12/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import Foundation

struct Pokemon {
    var number:Int?
    var name:String?
    var url:String?
}
