//
//  ViewController.swift
//  Hello World
//
//  Created by Daniel Macedo on 25/11/16.
//  Copyright © 2016 Daniel Macedo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var myTextField: UITextField!
    
    @IBOutlet weak var myLabel: UILabel!

    @IBAction func copyTextToLabel(_ sender: UIButton) {
        
        self.myLabel.text = self.myTextField.text
        
        self.myTextField.resignFirstResponder()
    }
    
    @IBAction func showAlertWithText(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Alert", message: self.myTextField.text, preferredStyle: .alert)
        
        let closeAction = UIAlertAction(title: "Fechar", style: .cancel, handler: {(action:UIAlertAction) -> Void in
            print("Fechou!")
        })
        
        alertController.addAction(closeAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

